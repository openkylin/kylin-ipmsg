#include <QMainWindow>
#include <QRegExp>
#include <QDebug>
#include <QEvent>
#undef Unsorted
#include <QDir>
#include <QFileDialog>
#include <QFile>
#include <QDesktopServices>
#include <QHostInfo>
#include <QNetworkInterface>
#include "addrset.h"

AddrSet::AddrSet(QWidget *parent) : QWidget(parent) {}

AddrSet::~AddrSet()
{
    if (NULL != m_addrset) {
        delete m_addrset;
        m_addrset = NULL;
    }
}

AddrSet *AddrSet::getInstance()
{
    static AddrSet *m_addrset = nullptr;
    if (NULL == m_addrset) {
        m_addrset = new AddrSet();
    }
    return m_addrset;
}


//获取IP地址
QString AddrSet::getHostIpAddress()
{
    QString strIpAddress = "";
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // 获取第一个本主机的IPv4地址
    int nListSize = ipAddressesList.size();
    for (int i = 0; i < nListSize; ++i) {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost && ipAddressesList.at(i).toIPv4Address()) {
            strIpAddress = ipAddressesList.at(i).toString();
            break;
        }
    }
    // 如果没有找到，则以本地IP地址为IP
    if (strIpAddress.isEmpty())
        strIpAddress = QHostAddress(QHostAddress::LocalHost).toString();
    return strIpAddress;
}

//获取MAC地址
QString AddrSet::getHostMacAddress()
{
    QList<QNetworkInterface> nets = QNetworkInterface::allInterfaces(); // 获取所有网络接口列表
    int nCnt = nets.count();
    QString strMacAddr = "";
    for (int i = 0; i < nCnt; i++) {
        // 如果此网络接口被激活并且正在运行并且不是回环地址，则就是我们需要找的Mac地址
        if (nets[i].flags().testFlag(QNetworkInterface::IsUp) && nets[i].flags().testFlag(QNetworkInterface::IsRunning)
            && !nets[i].flags().testFlag(QNetworkInterface::IsLoopBack)) {
            strMacAddr = nets[i].hardwareAddress();
            break;
        }
    }
    return strMacAddr;
}

QString AddrSet::getLocalHost()
{
    //获取主机名
    QString localHost = QHostInfo::localHostName();
    return localHost;
}
