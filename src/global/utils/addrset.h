#ifndef ADDRSET_H
#define ADDRSET_H

#include <QWidget>
#include "view/localinfo/localinfo.h"

class AddrSet : public QWidget
{
    Q_OBJECT
public:
    explicit AddrSet(QWidget *parent = nullptr);
    ~AddrSet();
    QString getHostIpAddress();  //获取IP
    QString getHostMacAddress(); //获取Mac
    QString getLocalHost();      //获取主机名

    static AddrSet *getInstance();

private:
    AddrSet *m_addrset;
};

#endif // ADDRSET_H
