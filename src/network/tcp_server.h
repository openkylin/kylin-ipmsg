#ifndef __TCP_SERVER_H__
#define __TCP_SERVER_H__

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QHostAddress>
#include <QThread>

#include "global/declare/declare.h"
#include "global/utils/global_data.h"
#include "tcp_module.h"

class TcpServer : public QTcpServer
{
    Q_OBJECT

public:
    TcpServer();
    ~TcpServer();

protected:
    void incomingConnection(qintptr socketDescriptor);

private:
    int startListen(void);

    /* 对外模块信号 */
signals:
    void sigTcpServerRecvMsgSuccess(ChatMsgInfo *recv);
    void sigTcpServerSendMsgSuccess(ChatMsgInfo *send);
    void sigTcpServerRecvFileSuccess(ChatMsgInfo *recv);
    void sigTcpServerSendFileSuccess(ChatMsgInfo *send);
    void sigTcpServerSendFileProgress(ChatMsgInfo *send);
    void sigTcpServerSendMsgFailed(ChatMsgInfo *send);
    void sigTcpServerOffline(int friendId);
    void sigTcpServerMaintanceDb(g_tcpItem item);

private slots:
    void slotEstablishNewLink(qintptr socketDescriptor);
    void slotRecvMsgSuccess(ChatMsgInfo *recv);
    void slotSendMsgSuccess(ChatMsgInfo *send);
    void slotRecvFileSuccess(ChatMsgInfo *recv);
    void slotSendFileSuccess(ChatMsgInfo *send);
    void slotSendFileProgress(ChatMsgInfo *send);
    void slotMaintainTcpLinkAdd(g_tcpItem item);
    void slotMaintainTcpLinkDelete(QString uuid);
};

#endif
