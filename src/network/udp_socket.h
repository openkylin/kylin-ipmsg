#ifndef __UDP_SOCKET_H__
#define __UDP_SOCKET_H__

#define UDP_BROADCAST 0x01 // 广播
#define UDP_UNICAST 0x02   // 单播
#define UDP_MULTICAST 0x03 // 多播
#define UDP_GOODBYE 0x04   // 下线

#include <QObject>
#include <QUdpSocket>
#include <QTimer>
#include <QNetworkInterface>
#include <QMap>
#include <QDateTime>

#include "model/friendlistmodel.h"
#include "global/utils/globalutils.h"
#include "global/utils/global_data.h"

class UdpSocket : public QObject
{
    Q_OBJECT
public:
    UdpSocket();
    ~UdpSocket();

    void udpSocketBroadcast(bool flag);

private:
    QUdpSocket *m_udpSocket;
    QTimer *m_timer;

    // 好友上线时间列表 <好友uuid, 时间秒数>
    QMap<QString, qint64> m_friendOnlineTimeMap;
    QTimer *m_onlineTimer;
    int m_maxDiffSecs;

private:
    void init(void);
signals:
    // 添加或更新好友上线信息
    void addOnlineFriend(FriendInfoData *friendInfo);

    // 更新好友离线信息
    void updateOfflineFriend(FriendInfoData *friendInfo);

private slots:
    void slotSocketRead(void);
    void slotOnline(void);
    void slotOffline(void);

    // 检测好友上线时间
    void slotCheckOnline();
};

#endif
