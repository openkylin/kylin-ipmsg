#include <unistd.h>

#include "titleseting.h"
#include "titlebar.h"
#include "view/kyview.h"
#include <QMainWindow>
#include <QDebug>
#include <QMessageBox>
#undef Unsorted
#include <QDir>
#include <QFileDialog>
#include <QDirIterator>
#include <sys/stat.h>

#include <error.h>
#include <windowmanage.hpp>

#include "global/database/chatmsgdb.h"
#include "controller/control.h"

TitleSeting::TitleSeting(QWidget *parent) : QWidget(parent)
{
    //初始化
    setWidgetUi();

    // 设置组件样式
    setWidgetStyle();
}

void TitleSeting::setWidgetUi()
{
    this->setWindowFlag(Qt::Tool);
    this->setWindowModality(Qt::WindowModal);
    this->setWindowModality(Qt::ApplicationModal);

    /* 适配kysdk的窗管 */
    ::kabase::WindowManage::removeHeader(this);

    this->setAutoFillBackground(true);
    this->setBackgroundRole(QPalette::Base);

    m_labelCatalogue = new QLabel(this);      //目录名称
    m_labelTip = new QLabel(this);            //提示
    m_lineRoute = new QLineEdit(this);        //路径
    m_chooseRouteBtn = new QPushButton(this); //选择路径
    m_clearChatBtn = new QPushButton(this);   //清空聊天记录
    m_clearCacheBtn = new QPushButton(this);  //清空缓存

    m_titleBar = new TitleBar(false, false, false);
    m_titleBar->m_pMinimizeButton->hide();

    m_labelCatalogue->setFixedHeight(36);
    m_lineRoute->setFixedSize(356, 48);
    m_chooseRouteBtn->setFixedSize(356, 36);
    m_clearChatBtn->setFixedSize(356, 36);
    m_clearCacheBtn->setFixedSize(356, 36);

    m_mainLayout = new QVBoxLayout(this); // 设置页面标题布局
    m_vControlLayout = new QVBoxLayout(); // 总体控件纵向布局
    m_hControlLayout = new QHBoxLayout(); // 总体控件横向布局

    // 对组件进行布局
    m_vControlLayout->setSpacing(0);
    m_vControlLayout->addWidget(m_labelCatalogue, Qt::AlignBottom);
    m_vControlLayout->addSpacing(10);
    m_vControlLayout->addWidget(m_lineRoute);
    m_vControlLayout->addSpacing(2);
    m_vControlLayout->addWidget(m_labelTip);
    m_vControlLayout->addSpacing(3);
    m_vControlLayout->addWidget(m_chooseRouteBtn);
    m_vControlLayout->addSpacing(2);
    m_vControlLayout->addWidget(m_clearChatBtn);
    m_vControlLayout->addSpacing(2);
    m_vControlLayout->addWidget(m_clearCacheBtn);
    m_vControlLayout->addSpacing(2);
    m_vControlLayout->setMargin(0);

    m_hControlLayout->addStretch();
    m_hControlLayout->addLayout(m_vControlLayout);
    m_hControlLayout->addStretch();
    //    m_hLayout->setMargin(0);
    //    m_hLayout->setSpacing(0);

    m_mainLayout->setSpacing(0);
    m_mainLayout->addWidget(m_titleBar);
    m_mainLayout->addSpacing(10);
    m_mainLayout->addLayout(m_hControlLayout);
    m_mainLayout->addStretch();
    m_mainLayout->setMargin(0);

    connect(m_titleBar->m_pCloseButton, &QPushButton::clicked, this, &TitleSeting::close);
    connect(m_chooseRouteBtn, &QPushButton::clicked, this, &TitleSeting::updateRoute);
    connect(m_clearChatBtn, &QPushButton::clicked, this, &TitleSeting::clearChat);
    connect(m_clearCacheBtn, &QPushButton::clicked, this, &TitleSeting::clearCache);
}

void TitleSeting::setWidgetStyle()
{
    this->setFixedSize(420, 320);

    QFont fontLabel = GlobalData::getInstance()->getFontSize16px();
    m_labelCatalogue->setFont(fontLabel);
    m_labelCatalogue->setText(tr("File Save Directory"));

    QFont font = GlobalData::getInstance()->getFontSize14px();
    m_lineRoute->setFont(font);
    m_lineRoute->setFocusPolicy(Qt::NoFocus);
    m_lineRoute->setText(IniSettings::getInstance()->getFilePath()); //路径
    m_lineRoute->setTextMargins(8, 6, 8, 6);
    m_lineRoute->setCursorPosition(0);

    m_labelTip->setMargin(0);
    m_labelTip->setStyleSheet("color:#f44e50;font:14px;");
    getPower(m_lineRoute->text());

    m_chooseRouteBtn->setText(tr("Change Directory"));
    m_chooseRouteBtn->setFont(font);
    m_chooseRouteBtn->setProperty("useButtonPalette", true);
    m_chooseRouteBtn->setProperty("isWindowButton", 0x1);
    m_chooseRouteBtn->setProperty("useIconHighlightEffect", 0x2);
    //    m_ChooseRouteBtn->setFlat(true);
    // m_chooseRouteBtn->setStyleSheet("QPushButton{text-align: left;padding-left: 16px;}");

    m_clearChatBtn->setText(tr("Clear All Chat Messages"));
    m_clearChatBtn->setFont(font);
    m_clearChatBtn->setProperty("useButtonPalette", true);
    m_clearChatBtn->setProperty("isWindowButton", 0x1);
    m_clearChatBtn->setProperty("useIconHighlightEffect", 0x2);
    //    m_ClearChatBtn->setFlat(true);
    // m_clearChatBtn->setStyleSheet("QPushButton{text-align: left;padding-left: 16px;}");

    m_clearCacheBtn->setText(tr("Clear the Cache"));
    m_clearCacheBtn->setFont(font);
    m_clearCacheBtn->setProperty("useButtonPalette", true);
    m_clearCacheBtn->setProperty("isWindowButton", 0x1);
    m_clearCacheBtn->setProperty("useIconHighlightEffect", 0x2);
    //    m_ClearCacheBtn->setFlat(true);
    // m_clearCacheBtn->setStyleSheet("QPushButton{text-align: left;padding-left: 16px;}");
    m_clearCacheBtn->setAutoFillBackground(true);
    m_clearCacheBtn->setBackgroundRole(QPalette::Base);


    /* 监听字体变化 */
    connect(Control::getInstance(), &Control::sigFontChange, this, [=]() {
        QFont font14 = GlobalData::getInstance()->getFontSize14px();
        QFont font16 = GlobalData::getInstance()->getFontSize16px();

        m_titleBar->m_pFuncLabel->setFont(font14);
        m_labelCatalogue->setFont(font16);
        m_lineRoute->setFont(font14);
        m_chooseRouteBtn->setFont(font14);
        m_clearChatBtn->setFont(font14);
        m_clearCacheBtn->setFont(font14);
    });
    connect(GlobalData::getInstance(), &GlobalData::signalChangeFont, this, &TitleSeting::slotChangeFont);

    // 设置弹窗应用内居中
    QRect availableGeometry = KyView::getInstance()->geometry();
    this->move(availableGeometry.center() - this->rect().center());
}

// 初始化弹窗
void TitleSeting::setTipWid(QString text)
{
    m_tipWidget->setWindowFlag(Qt::Tool);
    m_tipWidget->setWindowModality(Qt::WindowModal);
    m_tipWidget->setWindowModality(Qt::ApplicationModal);

    /* 适配kysdk的窗管 */
    ::kabase::WindowManage::removeHeader(m_tipWidget);

    m_tipWidget->setAutoFillBackground(true);
    m_tipWidget->setBackgroundRole(QPalette::Base);

    m_tipWidText = new QLabel(m_tipWidget); //提示内容
    m_tipWidIcon = new QLabel(m_tipWidget); //提示图标

    m_vTipWidLayout = new QVBoxLayout(m_tipWidget); // 提示弹窗纵向布局
    m_hTipWidLayout = new QHBoxLayout();            // 提示弹窗横向布局

    m_hTipWidLayout->setSpacing(0);
    m_hTipWidLayout->addStretch();
    m_hTipWidLayout->addWidget(m_tipWidIcon);
    m_hTipWidLayout->addSpacing(9);
    m_hTipWidLayout->addWidget(m_tipWidText);
    m_hTipWidLayout->addStretch();
    m_hTipWidLayout->setMargin(0);

    m_vTipWidLayout->setContentsMargins(0, 0, 0, 0);
    m_vTipWidLayout->setSpacing(0);
    m_vTipWidLayout->addStretch();
    m_vTipWidLayout->addLayout(m_hTipWidLayout);
    m_vTipWidLayout->addStretch();
    m_tipWidget->setLayout(m_vTipWidLayout);

    //设置图标
    QIcon searchIcon = QIcon::fromTheme("ukui-dialog-success");
    m_tipWidIcon->setPixmap(searchIcon.pixmap(searchIcon.actualSize(QSize(22, 22))));
    m_tipWidIcon->setProperty("isWindowButton", 0x1);
    m_tipWidIcon->setProperty("useIconHighlightEffect", 0x2);
    m_tipWidIcon->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    m_tipWidIcon->setAttribute(Qt::WA_TranslucentBackground, true);

    // text为要显示的信息
    m_tipWidText->setText(text);

    int w = m_tipWidText->width() + m_tipWidIcon->width();
    //    m_tipWidget->setFixedSize(120 , 60);
    m_tipWidget->setFixedWidth(w);
    m_tipWidget->setFixedHeight(60);

    // 关于弹窗应用内居中
    QRect availableGeometry = this->geometry();
    m_tipWidget->move(availableGeometry.center() - m_tipWidget->rect().center());

    //设置定时器，到时自我销毁
    QTimer *timer = new QTimer(this);
    timer->start(1500);         //时间1.5秒
    timer->setSingleShot(true); //仅触发一次

    connect(timer, SIGNAL(timeout()), this, SLOT(timeDestroy()));
    connect(Control::getInstance(), &Control::sigFontChange, this, [=]() {
        QFont font14 = GlobalData::getInstance()->getFontSize14px();

        m_tipWidText->setFont(font14);
    });
}

/* 修改目录 */
void TitleSeting::updateRoute()
{
    /* SDK功能点：更改目录 */
    GlobalSizeData::SDKPointChangeDir();

    QString SavePath = QDir::toNativeSeparators(
        QFileDialog::getExistingDirectory(this, tr("Change Directory"), IniSettings::getInstance()->getFilePath()));
    if (!SavePath.isEmpty()) {
        m_lineRoute->setText(SavePath);
        m_lineRoute->setCursorPosition(0);

        if (!this->checkSavePath(SavePath)) {
            this->m_labelTip->setText(tr("The save path can only be a dir under the home dir!"));

            return;
        }

        if (!getPower(SavePath)) {
            return;
        }

        IniSettings::getInstance()->setFilePath(SavePath);

        m_tipWidget = new QDialog(); //提示框

        setTipWid(tr("Modified Successfully"));

        m_tipWidget->exec();
    }

    return;
}

bool TitleSeting::checkSavePath(QString path)
{
    if (path.isEmpty()) {
        return false;
    }

    QFileInfo fileInfo(path);
    if (!fileInfo.isDir()) {
        return false;
    }

    QString baseName = fileInfo.baseName();
    if (baseName.isEmpty() || baseName.at(0) == QChar('.')) {
        return false;
    }


    QString homePath = getenv("HOME");
    if (!path.contains(homePath) || path == homePath || path == (homePath + '/')) {
        return false;
    }

    return true;
}

/* 清空聊天信息 */
void TitleSeting::clearChat()
{
    /* sdk功能点：清空聊天记录 */
    GlobalSizeData::SDKPointClearChatRecord();

    QMessageBox *msg = new QMessageBox(this);
    msg->setWindowTitle(tr(""));
    msg->setText(tr("Clear all messages？"));
    msg->setIcon(QMessageBox::Question);
    QPushButton *sureBtn = msg->addButton(tr("Yes"), QMessageBox::RejectRole);
    msg->addButton(tr("No"), QMessageBox::AcceptRole);
    msg->exec();

    if ((QPushButton *)msg->clickedButton() == sureBtn) {

        ChatMsgDB::getInstance()->clearAllMsg();
        FriendListModel::getInstance()->clearAllMessage();
        FriendListView::getInstance()->clearAllMessages();

        // 设置弹窗
        m_tipWidget = new QDialog(); //提示框
        setTipWid(tr("Cleared"));
        m_tipWidget->exec();
    }
    return;
}

/* 清空缓存 */
void TitleSeting::clearCache()
{ 
    /* SDK功能点：清空缓存 */
    GlobalSizeData::SDKPointCleanCache();
    
    QMessageBox *msg = new QMessageBox(this);
    msg->setWindowTitle(tr(""));
    msg->setText(tr("Clean the cache information such as images/videos/documents?"));
    msg->setIcon(QMessageBox::Question);
    msg->addButton(tr("No"), QMessageBox::AcceptRole);
    QPushButton *sureBtn = msg->addButton(tr("Yes"), QMessageBox::RejectRole);
    msg->exec();

    if ((QPushButton *)msg->clickedButton() != sureBtn) {
        return;
    }
    // QString path = IniSettings::getInstance()->getFilePath() + "/sendFiles";
    QString path = IniSettings::getInstance()->getFilePath();
    QDir pathDir(path);

    if (pathDir.isEmpty()) {
        // 设置弹窗
        m_tipWidget = new QDialog(); //提示框
        setTipWid(tr("Clean Up Complete"));
        m_tipWidget->exec();

        return;
    }

    // 会删除文件发送目录 在文件管理器显示该目录时会返回上一级
    QDirIterator DirsIterator(path, QDir::Files | QDir::AllDirs | QDir::NoDotAndDotDot | QDir::Hidden,
                              QDirIterator::NoIteratorFlags);

    while (DirsIterator.hasNext()) {
        // 删除文件操作如果返回否，那它就是目录
        if (!pathDir.remove(DirsIterator.next())) {
            // 删除目录本身以及它下属所有的文件及目录
            QDir(DirsIterator.filePath()).removeRecursively();
        }
    }

    if (path == GlobalUtils::getDefaultPath()) {
        pathDir.mkdir(path + "/sendFiles");
    }

    // 设置弹窗
    m_tipWidget = new QDialog(); //提示框
    setTipWid(tr("Clean Up Complete"));
    m_tipWidget->exec();

    return;
}

/* 获取目录权限 */
bool TitleSeting::getPower(QString SavePath)
{
    struct stat buf;
    std::string stdStr = SavePath.toStdString();
    const char *str = stdStr.c_str();

    memset(&buf, 0x00, sizeof(struct stat));

    int ret = stat(str, &buf);
    if (ret) {
        printf("Waring : TitleSeting , getPower , get file stat fail . errno = %d , errstr = %s\n", errno,
               strerror(errno));
        return false;
    }

    if (getuid() != buf.st_uid) {
        if (getgid() != buf.st_uid) {
            if (buf.st_mode & S_IROTH && buf.st_mode & S_IWOTH && buf.st_mode & S_IXOTH) {
                m_labelTip->setText(tr(""));
            } else {
                m_labelTip->setText(tr("Please do not save the file in this directory"));
                return false;
            }
        } else {
            if (buf.st_mode & S_IRGRP && buf.st_mode & S_IWGRP && buf.st_mode & S_IXGRP) {
                m_labelTip->setText(tr(""));
            } else {
                m_labelTip->setText(tr("Please do not save the file in this directory"));
                return false;
            }
        }
    } else {
        if (buf.st_mode & S_IRUSR && buf.st_mode & S_IWUSR && buf.st_mode & S_IXUSR) {
            m_labelTip->setText(tr(""));
        } else {
            m_labelTip->setText(tr("Please do not save the file in this directory"));
            return false;
        }
    }

    return true;
}

/* 销毁弹窗 */
void TitleSeting::timeDestroy()
{
    m_tipWidget->hide();
}

void TitleSeting::slotChangeFont()
{
    m_chooseRouteBtn->setFont(GlobalData::getInstance()->m_fontName);
    m_clearChatBtn->setFont(GlobalData::getInstance()->m_fontName);
    m_clearCacheBtn->setFont(GlobalData::getInstance()->m_fontName);
    m_labelTip->setFont(GlobalData::getInstance()->m_fontName);
    return;
}
