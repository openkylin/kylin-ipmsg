#ifndef LOCALUPDATENAME_H
#define LOCALUPDATENAME_H

#include <QWidget>
#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QScreen>
#include <QGuiApplication>

#include "view/titlebar/titlebar.h"
#include "global/utils/inisetting.h"

class LocalUpdateName : public QWidget
{
    Q_OBJECT
public:
    explicit LocalUpdateName(int nameType, bool isAuto, QWidget *parent = nullptr);
    explicit LocalUpdateName(int nameType, int friendId, QWidget *parent = nullptr);

    ~LocalUpdateName();

    // 初始化数据
    void setInitData(int nameType, bool isAuto);
    void setInitData(int nameType, int friendId);

    // 初始化组件
    void setWidgetUi();

    // 设置组件样式
    void setWidgetStyle();

    QLabel *m_nameTypeLab;    // 目录名称
    QLineEdit *m_nameEdit;    // 名字
    QLabel *m_tipsLab;        // 提示
    QPushButton *m_cancelBtn; // 取消按钮
    QPushButton *m_sureBtn;   // 确定按钮

    QVBoxLayout *m_mainLayout;     // 设置页面标题布局
    QVBoxLayout *m_vControlLayout; // 总体控件纵向布局
    QHBoxLayout *m_hControlLayout; // 总体控件横向布局
    QHBoxLayout *m_hButtonLayout;  // 总体控件横向布局

    enum NameType {
        LocalName = 0,
        FriendName,
    };

private:
    TitleBar *m_titleBar; // 标题栏


    IniSettings *m_settings;

    bool m_isAuto;
    int m_nameType;
    int m_friendId;

    // 确认按钮槽函数
    void slotConfirmBtn();

    // 取消按钮槽函数
    void slotCancelBtn();

signals:
};

#endif // LOCALUPDATENAME_H
