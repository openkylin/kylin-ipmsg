#ifndef __CHAT_SEARCH_FILTER_H__
#define __CHAT_SEARCH_FILTER_H__

#include <QSortFilterProxyModel>

class ChatSearchFilter : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    ChatSearchFilter();
    ~ChatSearchFilter();

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const;
    bool judgeWhetherPicture(QString path) const;
    bool judgeWhetherLink(QString str) const;

private:
    int verify_file(char *const filename) const;  /* 代码规范 */
signals:

public slots:
};

#endif
