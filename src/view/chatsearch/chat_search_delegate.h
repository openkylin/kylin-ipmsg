#ifndef __CHAT_SEARCH_DELEGATE_H__
#define __CHAT_SEARCH_DELEGATE_H__

#include <QAbstractItemDelegate>
#include <QSize>
#include <QAbstractTextDocumentLayout>

#include "global/utils/global_data.h"
#include "view/chatmsg/chatmsg.h"
#include "view/chatsearch/chat_search.h"
#include <QStyleOptionViewItem>

class ChatMsg;
class ChatSearch;

class ChatSearchDelegate : public QAbstractItemDelegate
{
    Q_OBJECT
public:
    ChatSearchDelegate(ChatMsg *);
    ChatSearchDelegate(ChatMsg *chatMsg, ChatSearch *chatSearch);
    ~ChatSearchDelegate();

    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

protected:
private:
    ChatMsg *m_chatMsg;
    ChatSearch *m_chatSearch;

signals:

public slots:
};

#endif
