#ifndef SEARCHFRIENDDELEGATE_H
#define SEARCHFRIENDDELEGATE_H
#include <QObject>
#include <QItemDelegate>
#include <QStyledItemDelegate>
#include <QStyleOption>
#include <QModelIndex>
#include <QEvent>
#include <QAbstractItemDelegate>
#include <QDateTime>
#include <QPainter>
#include <QVariant>
#include <QRectF>
#include <QPoint>
#include <QDebug>
#include <QAbstractTextDocumentLayout>

#include "model/friendlistmodel.h"
#include "global/utils/global_data.h"

class SearchFriendDelegate : public QAbstractItemDelegate
{
    Q_OBJECT
public:
    explicit SearchFriendDelegate();
    ~SearchFriendDelegate();

    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif
