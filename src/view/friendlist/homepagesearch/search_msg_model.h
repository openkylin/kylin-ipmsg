#ifndef SEARCHMSGMODEL_H
#define SEARCHMSGMODEL_H

#include <QWidget>
#include <QStandardItemModel>
#include <QDateTime>
#include <QDebug>

#include "global/database/chatmsgdb.h"
#include "model/friendlistmodel.h"
#include "model/chatmsgmodel.h"

class SearchMsgModel : public QStandardItemModel
{
    Q_OBJECT

public:
    SearchMsgModel();
    ~SearchMsgModel();

    static SearchMsgModel *getInstance();

private:
    void getModel(void);
    void getSearchResult(QString);

    QMap<int, int> m_searchResult;
    QHash<int, QStandardItem *> m_msgIdItemsMap;

public slots:
    void slotSelect(QString);
};

#endif // SEARCHMSGMODEL_H
