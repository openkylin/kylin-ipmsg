#include "blankpage.h"
#include "global/utils/global_data.h"
/* 适配 kySDK  */
#include <gsettingmonitor.h>

BlankPage::BlankPage(QWidget *parent) : QWidget(parent)
{
    init();
    initGsetting();
}

BlankPage::~BlankPage() {}

void BlankPage::init()
{
    this->setMinimumSize(QSize(350, 506));
    this->setAttribute(Qt::WA_StyledBackground, true);
    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    //    this->setStyleSheet("background-color:#FFFFFF");
    m_blankLabel = new QLabel();
    QIcon blankIcon = QIcon(":/res/kylin-ipmsg.svg");
    m_blankLabel->setPixmap(blankIcon.pixmap(blankIcon.actualSize(QSize(100, 100))));

    // 空白页面布局
    m_hBlankLayout = new QHBoxLayout();
    m_vBlankLayout = new QVBoxLayout();

    m_hBlankLayout->addStretch();
    m_hBlankLayout->addWidget(m_blankLabel);
    m_hBlankLayout->addStretch();

    // m_vBlankLayout->addSpacing(160);
    m_vBlankLayout->addStretch();
    m_vBlankLayout->addLayout(m_hBlankLayout);
    m_vBlankLayout->addSpacing(50);
    m_vBlankLayout->addStretch();

    this->setLayout(m_vBlankLayout);
}

void BlankPage::initGsetting()
{
    // 主题颜色
    this->connect(kdk::GsettingMonitor::getInstance(), &kdk::GsettingMonitor::systemThemeChange, this, [=]() {
        GlobalData::getInstance()->getSystemTheme();
        changeTheme();
    });
}

void BlankPage::changeTheme()
{
    if (GlobalSizeData::THEME_COLOR == GlobalSizeData::UKUILight) {
        this->setStyleSheet("background-color:#FFFFFF");
    } else {
        this->setStyleSheet("background-color:#1d1d1d");
    }
}
