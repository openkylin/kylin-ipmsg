#include "searchmsglist.h"

SearchMsgList::SearchMsgList(QListView *parent) : QListView(parent)
{
    setViewUi();
}

SearchMsgList::~SearchMsgList() {}

// 单例，初始化返回指针
SearchMsgList *SearchMsgList::getInstance(QListView *parent)
{
    static SearchMsgList *instance = nullptr;

    if (instance == nullptr) {
        instance = new SearchMsgList(parent);
    }

    return instance;
}


// 初始化组件
void SearchMsgList::setViewUi()
{
    m_chatMsgDelegate = new SearchMsgDelegate();
    this->setItemDelegate(m_chatMsgDelegate);

    this->setModel(SearchMsgModel::getInstance());

    this->setSpacing(0);
    this->setDragEnabled(false);
    this->setFrameShape(QListView::NoFrame);
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    this->verticalScrollBar()->setDisabled(true);

    connect(this, &SearchMsgList::doubleClicked, this, &SearchMsgList::startChat);
}

// 根据字符串过滤好友聊天记录
void SearchMsgList::filterFriendByReg(QString regStr)
{
    /* sdk功能点打点：主界面搜索 */
    GlobalSizeData::SDKPointMainSearch();
    
    emit this->sigSelectMsg(regStr);

    return;
}

// 发起聊天
void SearchMsgList::startChat()
{
    // 将代理index转换为好友index
    QModelIndex friendIndex = this->currentIndex();
    int friendId = friendIndex.data(ChatMsgModel::FriendId).toInt();

    // 清空好友未读消息数
    FriendListModel::getInstance()->clearUnreadMsgNum(friendId);

    emit sigStartChat(friendId);
}
