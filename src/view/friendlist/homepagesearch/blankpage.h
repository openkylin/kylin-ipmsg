#ifndef BLANKPAGE_H
#define BLANKPAGE_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QIcon>

#include "global/utils/inisetting.h"
#include "view/common/globalsizedata.h"

class BlankPage : public QWidget
{
    Q_OBJECT

public:
    BlankPage(QWidget *parent = nullptr);
    ~BlankPage();
    void init();
    void initGsetting();
    void changeTheme();

private:
    QLabel *m_blankLabel;

    // 空白页面布局
    QHBoxLayout *m_hBlankLayout;
    QVBoxLayout *m_vBlankLayout;
};


#endif // BLANKPAGE_H
