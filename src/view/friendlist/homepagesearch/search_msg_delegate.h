#ifndef SEARCHMSGDELEGATE_H
#define SEARCHMSGDELEGATE_H

#include <QObject>
#include <QItemDelegate>
#include <QStyledItemDelegate>
#include <QStyleOption>
#include <QModelIndex>
#include <QEvent>
#include <QAbstractItemDelegate>
#include <QDateTime>
#include <QPainter>
#include <QVariant>
#include <QRectF>
#include <QPoint>
#include <QDebug>
#include <QAbstractTextDocumentLayout>

#include "model/chatmsgmodel.h"
#include "global/utils/global_data.h"

class SearchMsgDelegate : public QAbstractItemDelegate
{
    Q_OBJECT
public:
    SearchMsgDelegate();
    ~SearchMsgDelegate();

    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    bool findHash(int friendId, int msgCount) const;
    QHash<int, int> m_friendIDHash;
    QHash<int, int> countHash(int fid, int count) const;
};





#endif // SEARCHMSGDELEGATE_H
