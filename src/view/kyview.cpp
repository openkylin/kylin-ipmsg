/*
 * Copyright (C) 2020, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kyview.h"
#include "global/utils/inisetting.h"
#include "global/utils/addrset.h"
#include "controller/control.h"
#include <QMessageBox>
#include <KWindowEffects>
#include <gsettingmonitor.h>
#include <usermanual.h>
#include "windowmanage.hpp"

#include <QApplication>
#include <QDesktopWidget>

KyView::KyView(QWidget *parent) : QWidget(parent)
{
    kabase::WindowManage::getWindowId(&m_windowId);

    // 初始化组件
    setWidgetUi();

    // 设置组件样式
    setWidgetStyle();
}

KyView::~KyView()
{
    if (GlobalUtils::DEBUG_MODE) {
        qDebug() << "~KyView()";
    }

    Control::getInstance()->sayGoodbye();

    LocalInfo::getInstance()->deleteLater();
    FriendListView::getInstance()->deleteLater();
    m_trayIconWid->deleteLater();
    m_searchPage->deleteLater();
    m_blankWid->deleteLater();

    Control::getInstance()->deleteLater();
}

// 生成静态实例
KyView *KyView::getInstance()
{
    static KyView *instance = nullptr;
    if (nullptr == instance) {
        instance = new KyView();
    }
    return instance;
}

// 初始化组件
void KyView::setWidgetUi()
{
    // 初始化并监听gsetting
    initGsetting();

    this->setFixedSize(WINDOWW, WINDOWH);

    // mainWid = new QWidget(this);

    m_mainLayout = new QVBoxLayout();

    // 堆栈窗口
    m_stackedWid = new QStackedWidget();

    // 空白页
    m_blankWid = new BlankPage();

    // 标题栏
    m_titleBar = new TitleBar();
    m_titleBar->setFixedHeight(GlobalSizeData::TITLEBAR_HEIGHT);

    // 本机信息
    m_localInfo = LocalInfo::getInstance();
    m_localInfo->installEventFilter(this);

    // 好友列表
    m_friendView = FriendListView::getInstance();

    // 搜索列表
    m_searchPage = SearchPage::getInstance();

    //创建托盘图标
    m_trayIconWid = TrayIconWid::getInstance();
    // m_trayIconWid->installEventFilter(this);
    // m_trayIconWid->show();

    // 堆栈窗体
    m_stackedWid->addWidget(m_blankWid);
    m_stackedWid->addWidget(m_friendView);
    m_stackedWid->addWidget(m_searchPage);

    if (this->m_friendView->model()->rowCount() == 0) {
        m_stackedWid->setCurrentIndex(0);
    } else {
        m_stackedWid->setCurrentIndex(1);
    }

    // 将组件添加到布局中
    m_mainLayout->addWidget(m_titleBar, Qt::AlignTop);
    m_mainLayout->addWidget(m_localInfo, Qt::AlignTop);
    m_mainLayout->addWidget(m_stackedWid, Qt::AlignTop);
    m_mainLayout->setMargin(0);
    m_mainLayout->setSpacing(0);
    this->setLayout(m_mainLayout);

    connect(m_titleBar->m_pCloseButton, &QPushButton::clicked, this, &KyView::deleteLater);
    connect(this->m_searchPage, &SearchPage::sigSearchEmpty, this, &KyView::slotBlankInterface);
    connect(this->m_searchPage, &SearchPage::sigSearchExist, this, &KyView::slotChangeStatck);
    connect(this->m_friendView, &FriendListView::sigFriendState, this, &KyView::slotFriendInterface);
    connect(this->m_friendView, &FriendListView::sigFriendsEmpty, this, &KyView::slotBlankInterface);
    connect(this, &KyView::sigChangeBlank, this, &KyView::slotBlankInterface);
    connect(this, &KyView::sigChangeFriend, this, &KyView::slotFriendInterface);
    connect(this, &KyView::sigChangeSearch, this, &KyView::slotSearchInterface);

    switch (GlobalData::getInstance()->m_currentMode)
    {
        case CurrentMode::PCMode:
        slotChangePCMode();        
            break;
        case CurrentMode::HMode:
        slotChangeHScreenMode();
            break;
        case CurrentMode::VMode:
        slotChangeVScreenMode();
            break;
    }
    connect(GlobalData::getInstance(), &GlobalData::signalChangePC, this, &KyView::slotChangePCMode);
    connect(GlobalData::getInstance(), &GlobalData::signalChangeFlatH, this, &KyView::slotChangeHScreenMode);
    connect(GlobalData::getInstance(), &GlobalData::signalChangeFlatV, this, &KyView::slotChangeVScreenMode);
}

// 设置组件样式
void KyView::setWidgetStyle()
{
    this->setWindowTitle(tr("Messages"));

    //毛玻璃
    this->setProperty("useSystemStyleBlur", true);
    this->setAttribute(Qt::WA_TranslucentBackground, true);

    // 应用居中
    QScreen *screen = QGuiApplication::primaryScreen();
    this->move(screen->geometry().center() - this->rect().center());

    this->update();
    m_localInfo->changeTheme();
    m_searchPage->changeTheme();
    m_blankWid->changeTheme();
    TrayIconWid::getInstance()->changeTheme();
}

// 初始化并监听gsetting
void KyView::initGsetting()
{
    // 主题
    GlobalData::getInstance()->getSystemTheme();

    this->connect(kdk::GsettingMonitor::getInstance(), &kdk::GsettingMonitor::systemThemeChange, this, [=]() {
        GlobalData::getInstance()->getSystemTheme();
        m_localInfo->changeTheme();
        TrayIconWid::getInstance()->changeTheme();
        this->update();
    });

    // 透明度
    GlobalData::getInstance()->getSystemTransparency();

    this->connect(kdk::GsettingMonitor::getInstance(), &kdk::GsettingMonitor::systemTransparencyChange, this, [=]() {
        GlobalData::getInstance()->getSystemTransparency();
        transChange();
    });
    return;
}

// 显示主界面后检查本机昵称
void KyView::checkLocalName()
{
    this->m_localInfo->checkLocalName();
}

// 窗口关闭事件
void KyView::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);

    this->deleteLater();
}

// 透明度变化
void KyView::transChange()
{
    // m_titleBar->update();
    m_localInfo->update();
    m_trayIconWid->update();
}

// 绘制事件
void KyView::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    /* 反锯齿 */
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);

    // m_localInfo->update();
    // m_titleBar->update();
}

void KyView::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_F1) {
        // 帮助点击事件处理
        kdk::UserManual userManual;
        if (!userManual.callUserManual("kylin-ipmsg")) {
            qCritical() << "user manual call fail!";
        }
    }
}

// 事件过滤器
bool KyView::eventFilter(QObject *watch, QEvent *event)
{
    if (m_localInfo->eventFilter(watch, event)) {
        return QObject::eventFilter(watch, event);
    }
    return false;
}

// 切换堆栈窗口
void KyView::slotChangeStatck()
{
    if (this->m_localInfo->m_searchLineEdit->text() == "") {
        if (this->m_friendView->model()->rowCount() != 0) {
            emit sigChangeFriend();
        } else {
            emit sigChangeBlank();
        }
    } else {
        emit sigChangeSearch();
    }
    return;
}

void KyView::slotSearchInterface()
{
    m_stackedWid->setCurrentIndex(2);
}

void KyView::slotBlankInterface()
{
    m_stackedWid->setCurrentIndex(0);
}

void KyView::slotFriendInterface()
{
    m_stackedWid->setCurrentIndex(1);
}

quint32 KyView::getWindowId(void)
{
    return m_windowId;
}

void KyView::slotChangeHScreenMode()
{
    int h = GlobalData::getInstance()->m_currentSize.second;
    this->setFixedSize(WINDOWW_MAX_H_SCREEN, h);
    m_titleBar->slotChangeHScreenMode();
    m_localInfo->slotChangeFlatHSize();
    ::kabase::WindowManage::setMiddleOfScreen(this);

    return;
}

void KyView::slotChangeVScreenMode()
{
    int w = GlobalData::getInstance()->m_currentSize.first;
    int h = GlobalData::getInstance()->m_currentSize.second;
    this->setFixedSize(w, h);
    m_titleBar->slotChangeVScreenMode();
    m_localInfo->slotChangeFlatVSize();
    ::kabase::WindowManage::setMiddleOfScreen(this);
    
    return;
}

void KyView::slotChangePCMode()
{
    this->setFixedSize(WINDOWW, WINDOWH);
    m_titleBar->slotChangePCMode();
    m_localInfo->slotChangePCSize();
    ::kabase::WindowManage::setMiddleOfScreen(this);

    return;
}
