<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>ChatMsg</name>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="145"/>
        <source>Resend</source>
        <translation>བསྐྱར་དུ་ཞིབ་གཅོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="146"/>
        <source>Copy</source>
        <translation>འདྲ་བཤུས་</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="147"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="148"/>
        <source>Open Directory</source>
        <translation>སྒོ་འབྱེད་དཀར་ཆག</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="149"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="984"/>
        <source>Save As</source>
        <translation>གྲོན་ཆུང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="150"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="151"/>
        <source>Clear All</source>
        <translation>གཙང་སེལ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="224"/>
        <source>File</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="233"/>
        <source>Folder</source>
        <translation>ཡིག་སྣོད་</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="242"/>
        <source>Screen Shot</source>
        <translation>བརྙན་ཤེལ་གྱི་པར་ལེན</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="251"/>
        <source>History Message</source>
        <translation>ལོ་རྒྱུས་ཀྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="268"/>
        <source>Send</source>
        <translation>སྐུར་སྐྱེལ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="528"/>
        <source>Message send failed, please check whether IP connection is successful!</source>
        <translation>ཆ་འཕྲིན་བརྒྱུད་སྤྲོད་བྱེད་མ་ཐུབ་པས། ཤེས་བྱའི་ཐོན་དངོས་བདག་དབང་གི་འབྲེལ་མཐུད་ལེགས་འགྲུབ་བྱུང་ཡོད་མེད་</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="641"/>
        <source>Can not write file</source>
        <translation>ཡིག་ཆ་འབྲི་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="838"/>
        <source>No such file or directory!</source>
        <translation>འདི་ལྟ་བུའི་ཡིག་ཆ་དང་དཀར་ཆག་མེད།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1004"/>
        <source>Delete the currently selected message?</source>
        <translation>མིག་སྔར་བདམས་ཟིན་པའི་ཆ་འཕྲིན་བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1007"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1035"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1084"/>
        <source>folder</source>
        <translation>ཡིག་སྣོད་ནང་དུ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1006"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1034"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1032"/>
        <source>Clear all current messages?</source>
        <translation>མིག་སྔའི་ཆ་འཕྲིན་ཚང་མ་གཙང་སེལ་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1054"/>
        <source>Send Files</source>
        <translation>ཡིག་ཆ་བསྐུར་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1081"/>
        <source>Send Folders</source>
        <translation>ཡིག་སྣོད་སྐུར་སྐྱེལ་བྱེད་པ</translation>
    </message>
</context>
<context>
    <name>ChatSearch</name>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="289"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation type="obsolete">清空聊天记录</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="190"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="193"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="327"/>
        <source>All</source>
        <translation>ཚང་མ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="198"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="200"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="328"/>
        <source>File</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="204"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="209"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="329"/>
        <source>Image/Video</source>
        <translation>པར་རིས་དང་བརྙན་ཕབ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="214"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="216"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="330"/>
        <source>Link</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="221"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="222"/>
        <source>canael</source>
        <translation>ཁ་ལོ་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="227"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="232"/>
        <source>sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="239"/>
        <source>DeleteMenu</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="140"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="250"/>
        <source>Batch delete</source>
        <translation>ཁག་བགོས་ནས་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="66"/>
        <source>Chat content</source>
        <translation>ཁ་བརྡའི་ནང་དོན།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="141"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="252"/>
        <source>Clear all messages</source>
        <translation>ཆ་འཕྲིན་ཡོད་ཚད་གཙང་སེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="162"/>
        <source>Chat Content</source>
        <translation>ཁ་བརྡའི་ནང་དོན།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="290"/>
        <source>Choose Delete</source>
        <translation>བདམས་ནས་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="291"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="292"/>
        <source>Open Directory</source>
        <translation>སྒོ་འབྱེད་དཀར་ཆག</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="684"/>
        <source>No such file or directory!</source>
        <translation>འདི་ལྟ་བུའི་ཡིག་ཆ་དང་དཀར་ཆག་མེད།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="745"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="979"/>
        <source>Delete the currently selected message?</source>
        <translation>མིག་སྔར་བདམས་ཟིན་པའི་ཆ་འཕྲིན་བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="748"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="786"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="982"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="747"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="785"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="981"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="783"/>
        <source>Clear all current messages?</source>
        <translation>མིག་སྔའི་ཆ་འཕྲིན་ཚང་མ་གཙང་སེལ་བྱེད་དགོས་སམ།</translation>
    </message>
</context>
<context>
    <name>Control</name>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/controller/control.cpp" line="282"/>
        <source>Anonymous</source>
        <translation>མིང་མ་བཀོད་པའི་</translation>
    </message>
</context>
<context>
    <name>FriendInfoWid</name>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="159"/>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="186"/>
        <source>Messages</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="195"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="225"/>
        <source>Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="234"/>
        <source>IP Address</source>
        <translation>IPས་གནས།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="244"/>
        <source>Nickname</source>
        <translation>མཚང་མིང་།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="343"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
    </message>
</context>
<context>
    <name>FriendListView</name>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="96"/>
        <source>Start Chat</source>
        <translation>གླེང་མོལ་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="97"/>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="150"/>
        <source>Set to Top</source>
        <translation>རྩེ་མོར་གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="98"/>
        <source>Change Nickname</source>
        <translation>མིང་འདོགས་སྟངས་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="99"/>
        <source>View Info</source>
        <translation>ལྟ་ཀློག་གི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="100"/>
        <source>Delete Friend</source>
        <translation>གྲོགས་པོ་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="153"/>
        <source>Cancel the Top</source>
        <translation>གོང་རིམ་མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>KyView</name>
    <message>
        <location filename="../../src/view/kyview.cpp" line="154"/>
        <source>Messages</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
</context>
<context>
    <name>LocalInfo</name>
    <message>
        <source>Search</source>
        <translation type="vanished">འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="179"/>
        <source>Modify Name</source>
        <translation>མིང་ལ་བཟོ་བཅོས་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="188"/>
        <source>Open Directory</source>
        <translation>སྒོ་འབྱེད་དཀར་ཆག</translation>
    </message>
</context>
<context>
    <name>LocalUpdateName</name>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="159"/>
        <source>Set Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="160"/>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="211"/>
        <source>Please enter username</source>
        <translation>ཁྱེད་ཀྱིས་སྤྱོད་མཁན་གྱི་མིང་ནང་འཇུག་</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="163"/>
        <source>Change nickname</source>
        <translation>མཚང་མིང་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="164"/>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="213"/>
        <source>Please enter friend nickname</source>
        <translation>གྲོགས་པོའི་མཚང་མིང་ནང་དུ་འཇུག་རོགས།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="167"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="172"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="179"/>
        <source>Skip</source>
        <translation>བྲོས་བྱོལ་དུ་སོང་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="216"/>
        <source>The length of user name is less than 20 words</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་གི་རིང་ཚད་ཡིག་འབྲུ་20ལས་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="218"/>
        <source>Please do not enter special characters</source>
        <translation>དམིགས་བསལ་གྱི་ཡི་གེ་མ་ཞུགས་རོགས།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="121"/>
        <source>Settings</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="122"/>
        <source>Quit</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>SearchMsgDelegate</name>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/search_msg_delegate.cpp" line="47"/>
        <source> relevant chat records</source>
        <translation> འབྲེལ་ཡོད་ཁ་བརྡའི་ཟིན་ཐོ།</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="125"/>
        <source>Start Chat</source>
        <translation>གླེང་མོལ་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="126"/>
        <source>Set to Top</source>
        <translation>རྩེ་མོར་གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="127"/>
        <source>Change Nickname</source>
        <translation>མིང་འདོགས་སྟངས་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="128"/>
        <source>View Info</source>
        <translation>ལྟ་ཀློག་གི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="129"/>
        <source>Delete Friend</source>
        <translation>གྲོགས་པོ་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="167"/>
        <source>Friend</source>
        <translation>གྲོགས་པོ།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="171"/>
        <source>Chat Record</source>
        <translation>ཁ་བརྡའི་ཟིན་ཐོ།</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="161"/>
        <source>Minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="162"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="165"/>
        <source>Messages</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
</context>
<context>
    <name>TitleSeting</name>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="103"/>
        <source>File Save Directory</source>
        <translation>ཡིག་ཆ་ཉར་ཚགས་བྱས་པའི་དཀར་ཆག</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="116"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="236"/>
        <source>Change Directory</source>
        <translation>འགྱུར་ལྡོག་བྱུང་བའི་དཀར་ཆག</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="124"/>
        <source>Clear All Chat Messages</source>
        <translation>ཁ་བརྡའི་ཆ་འཕྲིན་ཚང་མ་གཙང་སེལ་བྱ་དགོས</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="132"/>
        <source>Clear the Cache</source>
        <translation>མྱུར་ཚད་གཙང་སེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Modified successfully</source>
        <translation type="vanished">修改成功</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="255"/>
        <source>Modified Successfully</source>
        <translation>བཟོ་བཅོས་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="296"/>
        <source>Clear all messages？</source>
        <translation>ཆ་འཕྲིན་ཚང་མ་གཙང་སེལ་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="299"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="326"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="298"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="327"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="242"/>
        <source>The save path can only be a dir under the home dir!</source>
        <translation>སྐྱོབ་པའི་ལམ་ཕྲན་ནི་ཁྱིམ་གྱི་ཟ་མ་ཟ་སའི་འོག་གི་ས་འདམ་ཞིག་རེད།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="310"/>
        <source>Cleared</source>
        <translation>གཙང་བཤེར་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="324"/>
        <source>Clean the cache information such as images/videos/documents?</source>
        <translation>པར་རིས་དང་། བརྙན་ཕབ། ཡིག་ཆ་སོགས་མགྱོགས་མྱུར་གྱི་ཆ་འཕྲིན་གཙང་བཤེར་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="340"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="364"/>
        <source>Clean Up Complete</source>
        <translation>གཙང་བཤེར་ལེགས་འགྲུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Clean up complete</source>
        <translation type="vanished">清理完成</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="391"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="398"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="406"/>
        <source>Please do not save the file in this directory</source>
        <translation>ཁྱོད་ཀྱིས་དཀར་ཆག་འདིའི་ནང་གི་ཡིག་ཆ་ཉར་ཚགས་མ་བྱེད།</translation>
    </message>
</context>
<context>
    <name>TrayIconWid</name>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="168"/>
        <source>Ignore</source>
        <translation>སྣང་མེད་དུ་བཞག་</translation>
    </message>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="135"/>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="179"/>
        <source>Messages</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="44"/>
        <source>Options</source>
        <translation>འདེམས་ཚན་</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="58"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="141"/>
        <source>Settings</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="60"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="139"/>
        <source>Theme</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="62"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="137"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="64"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="135"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="66"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="143"/>
        <source>Quit</source>
        <translation>ཕྱིར་འབུད་</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="74"/>
        <source>Follow the Theme</source>
        <translation>བརྗོད་བྱ་གཙོ་བོར་བརྩི་སྲུང་</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="78"/>
        <source>Light Theme</source>
        <translation>འོད་ཟེར་བརྗོད་བྱ་གཙོ་བོ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="82"/>
        <source>Dark Theme</source>
        <translation>མུན་ནག་གི་བརྗོད་བྱ་གཙོ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="194"/>
        <source>Version: </source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="195"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>ཆི་ལིན་གྱི་རྣམ་ཐར་གྱིས་ཁྱབ་ཆུང་དྲ་རྒྱའི་ནང་གི་ཡི་གེའི་ཁ་བརྡ་དང་ཡིག་ཆ་བརྒྱུད་གཏོང་གི་བྱེད་ནུས་འདོན་སྤྲོད་བྱས་ཡོད་པས།ཞབས་ཞུ་ཆས་འཛུགས་མི་དགོས།མི་མང་པོས་དུས་མཉམ་དུ་འབྲེལ་འདྲིས་དང་བསྡུ་ལེན་བྱེད་པར་རྒྱབ་སྐྱོར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">ཞབས་ཞུ་ &amp; རྒྱབ་སྐྱོར་: </translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.h" line="68"/>
        <source>Messages</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
</context>
</TS>
