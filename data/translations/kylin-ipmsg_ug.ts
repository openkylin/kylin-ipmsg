<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>ChatMsg</name>
    <message>
        <source>No</source>
        <translation>ياق</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>كۆچۈرۈش</translation>
    </message>
    <message>
        <source>File</source>
        <translation>ھۆججەت</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>ئېچىش</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>يوللاش</translation>
    </message>
    <message>
        <source>Delete the currently selected message?</source>
        <translation>نۆۋەتتە تاللانغان ئۇچۇرنى ئۆچۈرىۋېتەمسىز؟</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation>ھۆججەت قىسقۇچ</translation>
    </message>
    <message>
        <source>Clear all current messages?</source>
        <translation>ھازىرقى بارلىق ئۇچۇرنى تازىلىدىڭمۇ؟</translation>
    </message>
    <message>
        <source>Resend</source>
        <translation>قايتا قايتار</translation>
    </message>
    <message>
        <source>Send Files</source>
        <translation>ھۆججەت يوللاش</translation>
    </message>
    <message>
        <source>folder</source>
        <translation>ھۆججەت قىسقۇچ</translation>
    </message>
    <message>
        <source>Can not write file</source>
        <translation>ھۆججەت يازغىلى بولمايدۇ</translation>
    </message>
    <message>
        <source>History Message</source>
        <translation>تارىخ ئۇچۇرى</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation>As نى ساقلاش</translation>
    </message>
    <message>
        <source>Message send failed, please check whether IP connection is successful!</source>
        <translation>ئۇچۇر يوللاش مەغلۇپ بولدى، IP ئۇلىنىشنىڭ مۇۋەپپەقىيەتلىك بولغان-بولمىغانلىقىنى تەكشۈرۈپ كۆرۈڭ!</translation>
    </message>
    <message>
        <source>No such file or directory!</source>
        <translation>بۇنداق ھۆججەت ياكى مۇندەرىجە يوق!</translation>
    </message>
    <message>
        <source>Send Folders</source>
        <translation>ھۆججەت قىسقۇچ ئەۋەتىش</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation>ھەممىنى يوقوتوش</translation>
    </message>
    <message>
        <source>Screen Shot</source>
        <translation>ئېكران سۈرەت</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>مۇندەرىجىنى ئېچىش</translation>
    </message>
</context>
<context>
    <name>ChatSearch</name>
    <message>
        <source>No</source>
        <translation>ياق</translation>
    </message>
    <message>
        <source>All</source>
        <translation>ھەممە ئادەم</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
    <message>
        <source>File</source>
        <translation>ھۆججەت</translation>
    </message>
    <message>
        <source>Link</source>
        <translation>ئۇلىنىش</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>ئېچىش</translation>
    </message>
    <message>
        <source>sure</source>
        <translation>ئەلۋەتتە</translation>
    </message>
    <message>
        <source>Batch delete</source>
        <translation>توپ ئۆچۈرۈش</translation>
    </message>
    <message>
        <source>Delete the currently selected message?</source>
        <translation>نۆۋەتتە تاللانغان ئۇچۇرنى ئۆچۈرىۋېتەمسىز؟</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <source>Clear all current messages?</source>
        <translation>ھازىرقى بارلىق ئۇچۇرنى تازىلىدىڭمۇ؟</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">ئىزدە</translation>
    </message>
    <message>
        <source>canael</source>
        <translation>كانال</translation>
    </message>
    <message>
        <source>Image/Video</source>
        <translation>رەسىم/ۋىدېئو</translation>
    </message>
    <message>
        <source>No such file or directory!</source>
        <translation>بۇنداق ھۆججەت ياكى مۇندەرىجە يوق!</translation>
    </message>
    <message>
        <source>DeleteMenu</source>
        <translation>DeleteMenu</translation>
    </message>
    <message>
        <source>Choose Delete</source>
        <translation>ئۆچۈرۈشنى تاللاڭ</translation>
    </message>
    <message>
        <source>Clear all messages</source>
        <translation>بارلىق مەسېجلەرنى تازىلاش</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>مۇندەرىجىنى ئېچىش</translation>
    </message>
    <message>
        <source>Chat Content</source>
        <translation>پاراڭ مەزمۇنى</translation>
    </message>
    <message>
        <source>Chat content</source>
        <translation>پاراڭ مەزمۇنى</translation>
    </message>
</context>
<context>
    <name>Control</name>
    <message>
        <source>Anonymous</source>
        <translation>نامسىز</translation>
    </message>
</context>
<context>
    <name>FriendInfoWid</name>
    <message>
        <source>Add</source>
        <translation>قوش</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation>IP ئادرېسى</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>ئىشلەتكۈچى نامى</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>ئۇچۇر-خەۋەر</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>تەخەللۇسلار</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished">ياپ</translation>
    </message>
</context>
<context>
    <name>FriendListView</name>
    <message>
        <source>Cancel the Top</source>
        <translation>چوققا كۆرۈنمىسۇن كۆرۈشنى بىكار قىلىش</translation>
    </message>
    <message>
        <source>Set to Top</source>
        <translation>چوققا قىلىپ بېكىتىش</translation>
    </message>
    <message>
        <source>Change Nickname</source>
        <translation>تەخەللۇس ئۆزگەرتىش</translation>
    </message>
    <message>
        <source>Start Chat</source>
        <translation>پاراڭنى باشلاش</translation>
    </message>
    <message>
        <source>View Info</source>
        <translation>ئۇچۇرنى كۆرۈش</translation>
    </message>
    <message>
        <source>Delete Friend</source>
        <translation>دوست ئۆچۈرۈش</translation>
    </message>
</context>
<context>
    <name>KyView</name>
    <message>
        <source>Messages</source>
        <translation>ئۇچۇر-خەۋەر</translation>
    </message>
</context>
<context>
    <name>LocalInfo</name>
    <message>
        <source>Search</source>
        <translation type="vanished">ئىزدە</translation>
    </message>
    <message>
        <source>Modify Name</source>
        <translation>نامىنى ئۆزگەرتىش</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>مۇندەرىجىنى ئېچىش</translation>
    </message>
</context>
<context>
    <name>LocalUpdateName</name>
    <message>
        <source>Skip</source>
        <translation>ئاتلاندۇق</translation>
    </message>
    <message>
        <source>Please enter friend nickname</source>
        <translation>دوست تور نامىنى كىرگۈزۈڭ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Please do not enter special characters</source>
        <translation>ئالاھىدە پېرسوناژلارنى كىرگۈزمەڭ</translation>
    </message>
    <message>
        <source>Change nickname</source>
        <translation>تور نامىنى ئۆزگەرتىش</translation>
    </message>
    <message>
        <source>Please enter username</source>
        <translation>ئىشلەتكۈچى نامىنى كىرگۈزۈڭ</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <source>The length of user name is less than 20 words</source>
        <translation>ئىشلەتكۈچى نامىنىڭ ئۇزۇنلۇقى 20 خەتكە يەتمەيدۇ</translation>
    </message>
    <message>
        <source>Set Username</source>
        <translation>ئىشلەتكۈچى نامىنى بەلگىلەش</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Settings</source>
        <translation>تەڭشەكلەر</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>ياپ</translation>
    </message>
</context>
<context>
    <name>SearchMsgDelegate</name>
    <message>
        <source> relevant chat records</source>
        <translation> مۇناسىۋەتلىك پاراڭلىشىش خاتىرىسى</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <source>Friend</source>
        <translation>ئاداش</translation>
    </message>
    <message>
        <source>Chat Record</source>
        <translation>پاراڭ خاتىرىسى</translation>
    </message>
    <message>
        <source>Set to Top</source>
        <translation>چوققا قىلىپ بېكىتىش</translation>
    </message>
    <message>
        <source>Change Nickname</source>
        <translation>تەخەللۇس ئۆزگەرتىش</translation>
    </message>
    <message>
        <source>Start Chat</source>
        <translation>پاراڭنى باشلاش</translation>
    </message>
    <message>
        <source>View Info</source>
        <translation>ئۇچۇرنى كۆرۈش</translation>
    </message>
    <message>
        <source>Delete Friend</source>
        <translation>دوست ئۆچۈرۈش</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>كىچىكلىتىش</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>ئۇچۇر-خەۋەر</translation>
    </message>
</context>
<context>
    <name>TitleSeting</name>
    <message>
        <source>No</source>
        <translation>ياق</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
    <message>
        <source>Clear All Chat Messages</source>
        <translation>بارلىق پاراڭلىشىش مەسېجلىرىنى تازىلاش</translation>
    </message>
    <message>
        <source>Clear all messages？</source>
        <translation>بارلىق ئۇچۇرنى تازىلىدىڭمۇ؟</translation>
    </message>
    <message>
        <source>Modified Successfully</source>
        <translation>مۇۋەپپىقىيەتلىك ئۆزگەرتىلدى</translation>
    </message>
    <message>
        <source>Clean Up Complete</source>
        <translation>تازىلاش تاماملاش</translation>
    </message>
    <message>
        <source>Cleared</source>
        <translation>تازىلاندى</translation>
    </message>
    <message>
        <source>The save path can only be a dir under the home dir!</source>
        <translation>تېجەش يولى پەقەت ئۆي دەرۋازى ئاستىدىكى بىر لاي بولالايدۇ!</translation>
    </message>
    <message>
        <source>Change Directory</source>
        <translation>مۇندەرىجىنى ئۆزگەرتىش</translation>
    </message>
    <message>
        <source>Clear the Cache</source>
        <translation>Cache نى تازىلاش</translation>
    </message>
    <message>
        <source>File Save Directory</source>
        <translation>ھۆججەت ساقلاش مۇندەرىجىسى</translation>
    </message>
    <message>
        <source>Please do not save the file in this directory</source>
        <translation>ھۆججەتنى بۇ مۇندەرىجىگە ساقلىۋالماڭلار</translation>
    </message>
    <message>
        <source>Clean the cache information such as images/videos/documents?</source>
        <translation>رەسىم/سىن/ھۆججەت قاتارلىق كاكاپ ئۇچۇرىنى تازىلىۋېلىڭ؟</translation>
    </message>
</context>
<context>
    <name>TrayIconWid</name>
    <message>
        <source>Ignore</source>
        <translation>نەزەردىن ساقىت قىلىش</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>ئۇچۇر-خەۋەر</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>تاللاش تۈرى</translation>
    </message>
    <message>
        <source>About</source>
        <translation>ھەققىدە</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>چېكىنىپ چىقىش</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>ئۇسلۇب</translation>
    </message>
    <message>
        <source>Follow the Theme</source>
        <translation>ئۇسلۇبقا ئەمەل قىلىش</translation>
    </message>
    <message>
        <source>Light Theme</source>
        <translation>يورۇقلۇق ئۇسلۇبى</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">مۇلازىمەت &gt; قوللاش: </translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>نەشرى: </translation>
    </message>
    <message>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>ئۇچۇر LAN دا قىسقا پاراڭلىشىش ۋە ھۆججەت يۆتكەش ئىقتىدارى بىلەن تەمىنلەيدۇ. مۇلازىمىتېر قۇرۇشنىڭ ھاجىتى يوق. ئۇ كۆپ ئادەمنىڭ بىرلا ۋاقىتتا ھەمكارلىشىۋېلىشىنى ۋە پاراللېل يوللاش ۋە قوبۇل قىلىشنى قوللايدۇ.</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>ئۇچۇر-خەۋەر</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>تەڭشەكلەر</translation>
    </message>
    <message>
        <source>Dark Theme</source>
        <translation>قېنىق ئۇسلۇب</translation>
    </message>
</context>
</TS>
