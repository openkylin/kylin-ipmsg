<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>ChatMsg</name>
    <message>
        <source>No</source>
        <translation>Жоқ</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Иә</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Көшіру</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Ашу</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Жіберу</translation>
    </message>
    <message>
        <source>Delete the currently selected message?</source>
        <translation>Таңдалған хабарды жою қажет пе?</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Өшіру</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation>Қапшық</translation>
    </message>
    <message>
        <source>Clear all current messages?</source>
        <translation>Барлық ағымдағы хабарларды тазаласын ба?</translation>
    </message>
    <message>
        <source>Resend</source>
        <translation>Резенд</translation>
    </message>
    <message>
        <source>Send Files</source>
        <translation>Файлдарды жіберу</translation>
    </message>
    <message>
        <source>folder</source>
        <translation>қапшық</translation>
    </message>
    <message>
        <source>Can not write file</source>
        <translation>Файлды жазу мүмкін емес</translation>
    </message>
    <message>
        <source>History Message</source>
        <translation>Журнал хабары</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation>Басқаша сақтау</translation>
    </message>
    <message>
        <source>Message send failed, please check whether IP connection is successful!</source>
        <translation>Хабар жіберілмеді, IP байланысының сәтті екенін тексеріңіз!</translation>
    </message>
    <message>
        <source>No such file or directory!</source>
        <translation>Ондай файл немесе каталог жоқ!</translation>
    </message>
    <message>
        <source>Send Folders</source>
        <translation>Қапшықтарды жіберу</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation>Барлығын жою</translation>
    </message>
    <message>
        <source>Screen Shot</source>
        <translation>Скриншот</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>Каталогты ашу</translation>
    </message>
</context>
<context>
    <name>ChatSearch</name>
    <message>
        <source>No</source>
        <translation>Жоқ</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Барлығы</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Иә</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <source>Link</source>
        <translation>Сілтеме</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Ашу</translation>
    </message>
    <message>
        <source>sure</source>
        <translation>сенімді</translation>
    </message>
    <message>
        <source>Batch delete</source>
        <translation>Партияны жою</translation>
    </message>
    <message>
        <source>Delete the currently selected message?</source>
        <translation>Таңдалған хабарды жою қажет пе?</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Өшіру</translation>
    </message>
    <message>
        <source>Clear all current messages?</source>
        <translation>Барлық ағымдағы хабарларды тазаласын ба?</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Іздеу</translation>
    </message>
    <message>
        <source>canael</source>
        <translation>канаэль</translation>
    </message>
    <message>
        <source>Image/Video</source>
        <translation>Сурет/Бейне</translation>
    </message>
    <message>
        <source>No such file or directory!</source>
        <translation>Ондай файл немесе каталог жоқ!</translation>
    </message>
    <message>
        <source>DeleteMenu</source>
        <translation>DeleteMenu</translation>
    </message>
    <message>
        <source>Choose Delete</source>
        <translation>Жою пәрменін таңдау</translation>
    </message>
    <message>
        <source>Clear all messages</source>
        <translation>Барлық хабарларды тазалау</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>Каталогты ашу</translation>
    </message>
    <message>
        <source>Chat Content</source>
        <translation>Чат мазмұны</translation>
    </message>
    <message>
        <source>Chat content</source>
        <translation>Чат мазмұны</translation>
    </message>
</context>
<context>
    <name>Control</name>
    <message>
        <source>Anonymous</source>
        <translation>Анонимді</translation>
    </message>
</context>
<context>
    <name>FriendInfoWid</name>
    <message>
        <source>Add</source>
        <translation>Қосу</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation>IP адресі</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Пайдаланушы аты</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Хабарлар</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Лақап аты</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished">Жабу</translation>
    </message>
</context>
<context>
    <name>FriendListView</name>
    <message>
        <source>Cancel the Top</source>
        <translation>Жоғарғы жағынан бас тарту</translation>
    </message>
    <message>
        <source>Set to Top</source>
        <translation>Жоғарыға орнату</translation>
    </message>
    <message>
        <source>Change Nickname</source>
        <translation>Лақап атын өзгерту</translation>
    </message>
    <message>
        <source>Start Chat</source>
        <translation>Чат бастау</translation>
    </message>
    <message>
        <source>View Info</source>
        <translation>Ақпаратты қарау</translation>
    </message>
    <message>
        <source>Delete Friend</source>
        <translation>Досты өшіру</translation>
    </message>
</context>
<context>
    <name>KyView</name>
    <message>
        <source>Messages</source>
        <translation>Хабарлар</translation>
    </message>
</context>
<context>
    <name>LocalInfo</name>
    <message>
        <source>Search</source>
        <translation type="vanished">Іздеу</translation>
    </message>
    <message>
        <source>Modify Name</source>
        <translation>Атауын өзгерту</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>Каталогты ашу</translation>
    </message>
</context>
<context>
    <name>LocalUpdateName</name>
    <message>
        <source>Skip</source>
        <translation>Өткізіп жіберу</translation>
    </message>
    <message>
        <source>Please enter friend nickname</source>
        <translation>Досыңыздың лақап атын енгізіңіз</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <source>Please do not enter special characters</source>
        <translation>Арнайы таңбалар енгізбеуіңізді өтінемін</translation>
    </message>
    <message>
        <source>Change nickname</source>
        <translation>Лақап атын өзгерту</translation>
    </message>
    <message>
        <source>Please enter username</source>
        <translation>Пайдаланушы атын енгізіңіз</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>Растау</translation>
    </message>
    <message>
        <source>The length of user name is less than 20 words</source>
        <translation>Пайдаланушы атауының ұзындығы 20 сөзден кем</translation>
    </message>
    <message>
        <source>Set Username</source>
        <translation>Пайдаланушы атын орнату</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Settings</source>
        <translation>Параметрлер</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Жабу</translation>
    </message>
</context>
<context>
    <name>SearchMsgDelegate</name>
    <message>
        <source> relevant chat records</source>
        <translation> тиісті чат жазбалары</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <source>Friend</source>
        <translation>Дос</translation>
    </message>
    <message>
        <source>Chat Record</source>
        <translation>Чат жазбасы</translation>
    </message>
    <message>
        <source>Set to Top</source>
        <translation>Жоғарыға орнату</translation>
    </message>
    <message>
        <source>Change Nickname</source>
        <translation>Лақап атын өзгерту</translation>
    </message>
    <message>
        <source>Start Chat</source>
        <translation>Чат бастау</translation>
    </message>
    <message>
        <source>View Info</source>
        <translation>Ақпаратты қарау</translation>
    </message>
    <message>
        <source>Delete Friend</source>
        <translation>Досты өшіру</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>Кішірейту</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Хабарлар</translation>
    </message>
</context>
<context>
    <name>TitleSeting</name>
    <message>
        <source>No</source>
        <translation>Жоқ</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Иә</translation>
    </message>
    <message>
        <source>Clear All Chat Messages</source>
        <translation>Барлық чат хабарларын тазалау</translation>
    </message>
    <message>
        <source>Clear all messages？</source>
        <translation>Барлық хабарларды тазалаңдар ма?</translation>
    </message>
    <message>
        <source>Modified Successfully</source>
        <translation>Сәтті өзгертілген</translation>
    </message>
    <message>
        <source>Clean Up Complete</source>
        <translation>Аяқтауды тазалау</translation>
    </message>
    <message>
        <source>Cleared</source>
        <translation>Тазаланған</translation>
    </message>
    <message>
        <source>The save path can only be a dir under the home dir!</source>
        <translation>Сақтау жолы үй дирінің астындағы кір ғана болуы мүмкін!</translation>
    </message>
    <message>
        <source>Change Directory</source>
        <translation>Каталогты өзгерту</translation>
    </message>
    <message>
        <source>Clear the Cache</source>
        <translation>Кэшті тазалау</translation>
    </message>
    <message>
        <source>File Save Directory</source>
        <translation>Файлды сақтау каталогы</translation>
    </message>
    <message>
        <source>Please do not save the file in this directory</source>
        <translation>Файлды осы каталогта сақтамауыңызды өтінемін</translation>
    </message>
    <message>
        <source>Clean the cache information such as images/videos/documents?</source>
        <translation>Суреттер/бейнелер/құжаттар сияқты кэш ақпаратын тазаласын ба?</translation>
    </message>
</context>
<context>
    <name>TrayIconWid</name>
    <message>
        <source>Ignore</source>
        <translation>Елемеу</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Хабарлар</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Help</source>
        <translation>Анықтама</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Опциялар</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Шамамен</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Шығу</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Тақырып</translation>
    </message>
    <message>
        <source>Follow the Theme</source>
        <translation>Тақырыпты бақылау</translation>
    </message>
    <message>
        <source>Light Theme</source>
        <translation>Жарық тақырыбы</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">Қызмет және қолдау: </translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>Нұсқасы: </translation>
    </message>
    <message>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>Хабар LAN-да мәтіндік чат және файлды беру функцияларын қамтамасыз етеді. Сервер құрудың қажеті жоқ. Ол бірнеше адамның бір мезгілде өзара әрекеттесіп, параллель жіберіп, қабылдауын қолдайды.</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Хабарлар</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Параметрлер</translation>
    </message>
    <message>
        <source>Dark Theme</source>
        <translation>Қараңғы тақырып</translation>
    </message>
</context>
</TS>
