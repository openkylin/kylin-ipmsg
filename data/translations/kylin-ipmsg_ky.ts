<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>ChatMsg</name>
    <message>
        <source>No</source>
        <translation>Жок</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Ооба</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Көчүрмө</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Ачуу</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Жөнөтүү</translation>
    </message>
    <message>
        <source>Delete the currently selected message?</source>
        <translation>Учурда тандалып алынган билдирүүнү жоготобу?</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Жоготуу</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation>Папка</translation>
    </message>
    <message>
        <source>Clear all current messages?</source>
        <translation>Бардык учурдагы билдирүүлөрдү тазалоо?</translation>
    </message>
    <message>
        <source>Resend</source>
        <translation>Рейнд</translation>
    </message>
    <message>
        <source>Send Files</source>
        <translation>Файлдарды жөнөтүү</translation>
    </message>
    <message>
        <source>folder</source>
        <translation>Папка</translation>
    </message>
    <message>
        <source>Can not write file</source>
        <translation>Файлды жазууга болбоду</translation>
    </message>
    <message>
        <source>History Message</source>
        <translation>Тарых кабары</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation>Куткаруу</translation>
    </message>
    <message>
        <source>Message send failed, please check whether IP connection is successful!</source>
        <translation>Билдирүү жөнөтүү ишке ашпады, сураныч, IP туташтыруу ийгиликтүү экенин текшерип көргүлө!</translation>
    </message>
    <message>
        <source>No such file or directory!</source>
        <translation>Мындай файл же каталог жок!</translation>
    </message>
    <message>
        <source>Send Folders</source>
        <translation>Папкаларды жөнөтүү</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation>Баарын тазалоо</translation>
    </message>
    <message>
        <source>Screen Shot</source>
        <translation>Экрандын сүрөтү</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>Ачык каталог</translation>
    </message>
</context>
<context>
    <name>ChatSearch</name>
    <message>
        <source>No</source>
        <translation>Жок</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Баары</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Ооба</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <source>Link</source>
        <translation>Шилтеме</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Ачуу</translation>
    </message>
    <message>
        <source>sure</source>
        <translation>албетте</translation>
    </message>
    <message>
        <source>Batch delete</source>
        <translation>Партияны жоготуу</translation>
    </message>
    <message>
        <source>Delete the currently selected message?</source>
        <translation>Учурда тандалып алынган билдирүүнү жоготобу?</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Жоготуу</translation>
    </message>
    <message>
        <source>Clear all current messages?</source>
        <translation>Бардык учурдагы билдирүүлөрдү тазалоо?</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Издөө</translation>
    </message>
    <message>
        <source>canael</source>
        <translation>канель</translation>
    </message>
    <message>
        <source>Image/Video</source>
        <translation>Сүрөт /Видео</translation>
    </message>
    <message>
        <source>No such file or directory!</source>
        <translation>Мындай файл же каталог жок!</translation>
    </message>
    <message>
        <source>DeleteMenu</source>
        <translation>ЖоготууМену</translation>
    </message>
    <message>
        <source>Choose Delete</source>
        <translation>Жоготууну тандоо</translation>
    </message>
    <message>
        <source>Clear all messages</source>
        <translation>Бардык билдирүүлөрдү тазалоо</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>Ачык каталог</translation>
    </message>
    <message>
        <source>Chat Content</source>
        <translation>Чат мазмуну</translation>
    </message>
    <message>
        <source>Chat content</source>
        <translation>Чат мазмуну</translation>
    </message>
</context>
<context>
    <name>Control</name>
    <message>
        <source>Anonymous</source>
        <translation>Анонимдүү</translation>
    </message>
</context>
<context>
    <name>FriendInfoWid</name>
    <message>
        <source>Add</source>
        <translation>Кошуу</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation>IP-дарек</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Колдонуучунун аты</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Билдирүүлөр</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Лақап ат</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished">Жабуу</translation>
    </message>
</context>
<context>
    <name>FriendListView</name>
    <message>
        <source>Cancel the Top</source>
        <translation>Топту жокко чыгаруу</translation>
    </message>
    <message>
        <source>Set to Top</source>
        <translation>Жогорку орунга коюлду</translation>
    </message>
    <message>
        <source>Change Nickname</source>
        <translation>Лақап атын өзгөртүү</translation>
    </message>
    <message>
        <source>Start Chat</source>
        <translation>Чат баштоо</translation>
    </message>
    <message>
        <source>View Info</source>
        <translation>Инфо көрүү</translation>
    </message>
    <message>
        <source>Delete Friend</source>
        <translation>Досуңду жоготуу</translation>
    </message>
</context>
<context>
    <name>KyView</name>
    <message>
        <source>Messages</source>
        <translation>Билдирүүлөр</translation>
    </message>
</context>
<context>
    <name>LocalInfo</name>
    <message>
        <source>Search</source>
        <translation type="vanished">Издөө</translation>
    </message>
    <message>
        <source>Modify Name</source>
        <translation>Аты-жөнүн өзгөртүү</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation>Ачык каталог</translation>
    </message>
</context>
<context>
    <name>LocalUpdateName</name>
    <message>
        <source>Skip</source>
        <translation>Өтүү</translation>
    </message>
    <message>
        <source>Please enter friend nickname</source>
        <translation>Сураныч, досу лақап атын киргизүү</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <source>Please do not enter special characters</source>
        <translation>Атайын символдорго кирбегиле</translation>
    </message>
    <message>
        <source>Change nickname</source>
        <translation>Лақап атын өзгөртүү</translation>
    </message>
    <message>
        <source>Please enter username</source>
        <translation>Колдонуучунун атын киргизиңиз</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>Ырастоосу</translation>
    </message>
    <message>
        <source>The length of user name is less than 20 words</source>
        <translation>Колдонуучунун атынын узундугу 20 сөздөн аз</translation>
    </message>
    <message>
        <source>Set Username</source>
        <translation>Username орнотуу</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Settings</source>
        <translation>Параметрлер</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Жабуу</translation>
    </message>
</context>
<context>
    <name>SearchMsgDelegate</name>
    <message>
        <source> relevant chat records</source>
        <translation> тиешелүү чат жазуулары</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <source>Friend</source>
        <translation>Досу</translation>
    </message>
    <message>
        <source>Chat Record</source>
        <translation>Чат жазуу</translation>
    </message>
    <message>
        <source>Set to Top</source>
        <translation>Жогорку орунга коюлду</translation>
    </message>
    <message>
        <source>Change Nickname</source>
        <translation>Лақап атын өзгөртүү</translation>
    </message>
    <message>
        <source>Start Chat</source>
        <translation>Чат баштоо</translation>
    </message>
    <message>
        <source>View Info</source>
        <translation>Инфо көрүү</translation>
    </message>
    <message>
        <source>Delete Friend</source>
        <translation>Досуңду жоготуу</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>Минималдуу</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Билдирүүлөр</translation>
    </message>
</context>
<context>
    <name>TitleSeting</name>
    <message>
        <source>No</source>
        <translation>Жок</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Ооба</translation>
    </message>
    <message>
        <source>Clear All Chat Messages</source>
        <translation>Бардык чат билдирүүлөрүн тазалоо</translation>
    </message>
    <message>
        <source>Clear all messages？</source>
        <translation>Бардык билдирүүлөрдү тазалоо?</translation>
    </message>
    <message>
        <source>Modified Successfully</source>
        <translation>Өзгөртүлүп берилди</translation>
    </message>
    <message>
        <source>Clean Up Complete</source>
        <translation>Толук тазалоо</translation>
    </message>
    <message>
        <source>Cleared</source>
        <translation>Тазаланды</translation>
    </message>
    <message>
        <source>The save path can only be a dir under the home dir!</source>
        <translation>Куткаруу жолу үй кир астында гана кир болушу мүмкүн!</translation>
    </message>
    <message>
        <source>Change Directory</source>
        <translation>Каталогду өзгөртүү</translation>
    </message>
    <message>
        <source>Clear the Cache</source>
        <translation>Кэшти тазалоо</translation>
    </message>
    <message>
        <source>File Save Directory</source>
        <translation>Каталогду сактоо файлы</translation>
    </message>
    <message>
        <source>Please do not save the file in this directory</source>
        <translation>Файлды бул каталогдо сактабаңыз</translation>
    </message>
    <message>
        <source>Clean the cache information such as images/videos/documents?</source>
        <translation>сүрөттөр / видео / документтер сыяктуу кэш маалымат тазалоо?</translation>
    </message>
</context>
<context>
    <name>TrayIconWid</name>
    <message>
        <source>Ignore</source>
        <translation>Четке кагуу</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Билдирүүлөр</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Help</source>
        <translation>Жардам</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Тандоо</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Жөнүндө</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Xtbʼan kan pa ruwiʼ</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Follow the Theme</source>
        <translation>Теманы ээрчигиле</translation>
    </message>
    <message>
        <source>Light Theme</source>
        <translation>Жарык темасы</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">Кызмат &amp;amp: </translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>Версиясы: </translation>
    </message>
    <message>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>Билдирүү ЛАН тексттик чат жана файл которуу милдеттерин берет. Сервер куруунун зарылдыгы жок. Ал бир эле учурда өз ара аракеттенүү жана параллель жөнөтүү жана алуу үчүн бир нече адамды колдойт.</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Билдирүүлөр</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Параметрлер</translation>
    </message>
    <message>
        <source>Dark Theme</source>
        <translation>Караңгы тема</translation>
    </message>
</context>
</TS>
