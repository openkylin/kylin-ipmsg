<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ChatMsg</name>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="145"/>
        <source>Resend</source>
        <translation>重新发送</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="146"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="147"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="148"/>
        <source>Open Directory</source>
        <translation>打开目录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="149"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="984"/>
        <source>Save As</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="150"/>
        <source>Delete</source>
        <translation>删除记录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="151"/>
        <source>Clear All</source>
        <translation>清空聊天记录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="224"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="233"/>
        <source>Folder</source>
        <translation>文件夹</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="242"/>
        <source>Screen Shot</source>
        <translation>截图</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="251"/>
        <source>History Message</source>
        <translation>历史记录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="268"/>
        <source>Send</source>
        <translation>发送</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="528"/>
        <source>Message send failed, please check whether IP connection is successful!</source>
        <translation>消息发送失败，请检查IP是否连接成功！</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="641"/>
        <source>Can not write file</source>
        <translation>无法写入文件</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="838"/>
        <source>No such file or directory!</source>
        <translation>此文件或文件夹不存在</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1004"/>
        <source>Delete the currently selected message?</source>
        <translation>是否删除当前所选记录？</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1007"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1035"/>
        <source>No</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1084"/>
        <source>folder</source>
        <translation>文件夹</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1006"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1034"/>
        <source>Yes</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1032"/>
        <source>Clear all current messages?</source>
        <translation>是否清空当前所有记录？</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1054"/>
        <source>Send Files</source>
        <translation>发送文件</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1081"/>
        <source>Send Folders</source>
        <translation>发送文件夹</translation>
    </message>
</context>
<context>
    <name>ChatSearch</name>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="289"/>
        <source>Delete</source>
        <translation>删除记录</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation type="obsolete">清空聊天记录</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="190"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="193"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="327"/>
        <source>All</source>
        <translation>全部</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="198"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="200"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="328"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="204"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="209"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="329"/>
        <source>Image/Video</source>
        <translation>图片/视频</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="214"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="216"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="330"/>
        <source>Link</source>
        <translation>链接</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="221"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="222"/>
        <source>canael</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="227"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="232"/>
        <source>sure</source>
        <translation>删除所选</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="239"/>
        <source>DeleteMenu</source>
        <translation>删除菜单</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="140"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="250"/>
        <source>Batch delete</source>
        <translation>批量删除</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="66"/>
        <source>Chat content</source>
        <translation>聊天内容</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="141"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="252"/>
        <source>Clear all messages</source>
        <translation>清空全部消息</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="162"/>
        <source>Chat Content</source>
        <translation>聊天内容</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="290"/>
        <source>Choose Delete</source>
        <translation>批量删除记录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="291"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="292"/>
        <source>Open Directory</source>
        <translation>打开目录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="684"/>
        <source>No such file or directory!</source>
        <translation>此文件或文件夹不存在</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="745"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="979"/>
        <source>Delete the currently selected message?</source>
        <translation>是否删除当前所选记录？</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="748"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="786"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="982"/>
        <source>No</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="747"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="785"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="981"/>
        <source>Yes</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="783"/>
        <source>Clear all current messages?</source>
        <translation>是否清空当前所有记录？</translation>
    </message>
</context>
<context>
    <name>Control</name>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/controller/control.cpp" line="282"/>
        <source>Anonymous</source>
        <translation>匿名</translation>
    </message>
</context>
<context>
    <name>FriendInfoWid</name>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="159"/>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="186"/>
        <source>Messages</source>
        <translation>传书</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="195"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="225"/>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="234"/>
        <source>IP Address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="244"/>
        <source>Nickname</source>
        <translation>备注名</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="343"/>
        <source>Add</source>
        <translation>添加备注</translation>
    </message>
</context>
<context>
    <name>FriendListView</name>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="96"/>
        <source>Start Chat</source>
        <translation>发起聊天</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="97"/>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="150"/>
        <source>Set to Top</source>
        <translation>设为置顶</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="98"/>
        <source>Change Nickname</source>
        <translation>修改好友备注</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="99"/>
        <source>View Info</source>
        <translation>查看资料</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="100"/>
        <source>Delete Friend</source>
        <translation>删除好友</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="153"/>
        <source>Cancel the Top</source>
        <translation>取消置顶</translation>
    </message>
</context>
<context>
    <name>KyView</name>
    <message>
        <location filename="../../src/view/kyview.cpp" line="154"/>
        <source>Messages</source>
        <translation>传书</translation>
    </message>
</context>
<context>
    <name>LocalInfo</name>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="179"/>
        <source>Modify Name</source>
        <translation>修改名字</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="188"/>
        <source>Open Directory</source>
        <translation>打开目录</translation>
    </message>
</context>
<context>
    <name>LocalUpdateName</name>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="159"/>
        <source>Set Username</source>
        <translation>设置用户名</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="160"/>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="211"/>
        <source>Please enter username</source>
        <translation>请输入用户名</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="163"/>
        <source>Change nickname</source>
        <translation>修改备注</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="164"/>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="213"/>
        <source>Please enter friend nickname</source>
        <translation>请输入好友备注</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="167"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="172"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="179"/>
        <source>Skip</source>
        <translation>跳过</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="216"/>
        <source>The length of user name is less than 20 words</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="218"/>
        <source>Please do not enter special characters</source>
        <translation>请勿输入特殊字符作为用户名</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="121"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="122"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>SearchMsgDelegate</name>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/search_msg_delegate.cpp" line="47"/>
        <source> relevant chat records</source>
        <translation>条相关聊天记录</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="125"/>
        <source>Start Chat</source>
        <translation>发起聊天</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="126"/>
        <source>Set to Top</source>
        <translation>设为置顶</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="127"/>
        <source>Change Nickname</source>
        <translation>修改好友备注</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="128"/>
        <source>View Info</source>
        <translation>查看资料</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="129"/>
        <source>Delete Friend</source>
        <translation>删除好友</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="167"/>
        <source>Friend</source>
        <translation>好友</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="171"/>
        <source>Chat Record</source>
        <translation>聊天记录</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="161"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="162"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="165"/>
        <source>Messages</source>
        <translation>传书</translation>
    </message>
</context>
<context>
    <name>TitleSeting</name>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="103"/>
        <source>File Save Directory</source>
        <translation>文件保存目录</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="116"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="236"/>
        <source>Change Directory</source>
        <translation>更改目录</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="124"/>
        <source>Clear All Chat Messages</source>
        <translation>清空聊天记录</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="132"/>
        <source>Clear the Cache</source>
        <translation>清理缓存</translation>
    </message>
    <message>
        <source>Modified successfully</source>
        <translation type="vanished">修改成功</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="255"/>
        <source>Modified Successfully</source>
        <translation>修改成功</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="296"/>
        <source>Clear all messages？</source>
        <translation>是否清空所有聊天记录？</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="299"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="326"/>
        <source>No</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="298"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="327"/>
        <source>Yes</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="242"/>
        <source>The save path can only be a dir under the home dir!</source>
        <translation>保存路径只能是家目录下的一个非隐藏目录!</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="310"/>
        <source>Cleared</source>
        <translation>已清空</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="324"/>
        <source>Clean the cache information such as images/videos/documents?</source>
        <translation>是否清理图片/视频/文档等缓存信息？</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="340"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="364"/>
        <source>Clean Up Complete</source>
        <translation>清理完成</translation>
    </message>
    <message>
        <source>Clean up complete</source>
        <translation type="vanished">清理完成</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="391"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="398"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="406"/>
        <source>Please do not save the file in this directory</source>
        <translation>请不要将文件保存在此目录中</translation>
    </message>
</context>
<context>
    <name>TrayIconWid</name>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="168"/>
        <source>Ignore</source>
        <translation>忽略消息</translation>
    </message>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="135"/>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="179"/>
        <source>Messages</source>
        <translation>传书</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="44"/>
        <source>Options</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="58"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="141"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="60"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="139"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="62"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="137"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="64"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="135"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="66"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="143"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="74"/>
        <source>Follow the Theme</source>
        <translation>跟随主题</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="78"/>
        <source>Light Theme</source>
        <translation>浅色主题</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="82"/>
        <source>Dark Theme</source>
        <translation>深色主题</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="194"/>
        <source>Version: </source>
        <translation>版本号：</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="195"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>麒麟传书提供局域网内的文字聊天以及文件传输功能，不需要搭建服务器，支持多人同时交互，收发并行。</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.h" line="68"/>
        <source>Messages</source>
        <translation>传书</translation>
    </message>
</context>
</TS>
