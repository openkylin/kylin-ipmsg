# Messages
## Summary

Transmittal is a cross platform and efficient text / file transmission tool. When users transfer files, they will compare with the local file names to avoid overwriting the original files and losing important information.

There is no server design for book transmission, and all functions are completed through the client

![Figure 1 Main interface of Book Transmission-big](image/1.png)
<br>

### Modify user name

Click"![](image/2.png)"icon to modify the user name.

![Figure 2 Modify user name-big](image/8.png)

### Modify friends notes

Right click the friend list to pop up a menu, where you can modify friend notes and view friend information.

![Figure 3 Modify friend notes-big](image/3.png)

![Figure 4 View friend info-big](image/4.png)

### Set up

Click the setting option in the menu bar to make corresponding settings.

![Figure 5 Setting interface-big](image/6.png)

### Send message

Double click the friends list to open the chat window. There are three buttons at the bottom of the chat window. You can chat with friends and transfer screenshots, files and folders.

![Figure 6 Friend chat window-big](image/7.png)

### Help and about

Click Help in the menu bar to open the help manual of the book.

Click "about" to view the information about the leaflet.