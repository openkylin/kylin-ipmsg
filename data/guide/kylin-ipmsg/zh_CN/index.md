# 传书
## 概 述
传书是一个跨平台、高效的文字/文件传输工具。在用户传输文件时，会与本地文件名相互比较，避免覆盖原有的文件，损失重要信息。

传书无服务器设计，所有功能通过客户端完成。

![图 1 传书主界面-big](image/1.png)
<br>

### 修改用户名

点击用户名旁边的“![](image/2.png)”图标可以修改用户名。

![图 2 修改用户名-big](image/8.png)

### 修改好友备注

右击好友列表会弹出菜单可以修改好友备注和查看好友信息

![图 3 修改好友备注-big](image/3.png)

![图 4 查看好友信息-big](image/4.png)

### 设 置

在菜单栏点击设置选项，可进行相应设置。

![图 5 设置界面-big](image/6.png)

### 发送消息

双击好友列表 , 可打开聊天窗口 ， 在聊天窗口底部有三个按钮 。可以进行好友聊天以及截图 、文件和文件夹的传输

![图 6 好友聊天窗口-big](image/7.png)

### 帮助和关于

在菜单栏中点击帮助可打开传书的帮助手册。

点击“关于”，可查看传书关于信息。
